﻿# -*- coding: utf-8 -*-


from lxml import etree
from validators import Validators, check_parameters
   
class PyKatus():  
   
   
   def __init__(self, xml_node = "carrinho"):
       self.validators = Validators()
       
       if xml_node:
           self.xml_node = etree.Element(xml_node)
        
        
     
   def monta_xml(self, parent, **kwargs):
       if isinstance(parent, etree._Element):
           node_parent = parent
       else:
           node_parent = etree.Element(parent)       
   
       for k,v in kwargs.items():
           node = etree.SubElement(node_parent, k)
           if isinstance(v, dict):
               self.monta_xml(node, **v)
           else:
               node.text = v
     
       return node_parent 
 
 
   @check_parameters
   def set_ambiente(self,ambiente):
   
        ambientes = dict(sandbox="dev", producao="www")
        
        try:
            return "https://%s.akatus.com/api/v1/carrinho.xml" %ambientes[ambiente] 
        except KeyError:
            raise ValueError("O ambiente escolhido eh invalido")
            
   @check_parameters
   def set_recebedor(self,token, email):
       
       self.validators.email(email)    
        
       self.monta_xml(self.xml_node, recebedor=dict(api_key=token, email=email)) 
       
       print etree.tostring(self.xml_node, pretty_print=True)    
       
       

   @check_parameters
   def set_produto(self,codigo, descricao, quantidade, preco, peso, frete, desconto):
        
       self.monta_xml(self.xml_node, produtos=dict(produto=dict(codigo=codigo, descricao=descricao,
                                                                 quantidade=quantidade, preco=preco,
                                                                 peso=peso, frete=frete, desconto=desconto))) 
       print etree.tostring(self.xml_node, pretty_print=True)          
       

       
   def all_tests(self):
      """Validacao da funcao set_ambiente
      >>> t = PyKatus("")
      >>> t.set_ambiente('sandbox')
      'https://dev.akatus.com/api/v1/carrinho.xml'
      
      >>> t.set_ambiente('producao')
      'https://www.akatus.com/api/v1/carrinho.xml'
      
      >>> t.set_ambiente('teste')
      Traceback (most recent call last):
      ValueError: O ambiente escolhido eh invalido
      
      >>> t.set_ambiente('')
      Traceback (most recent call last):
      ValueError: Voce deve informar um ambiente
     

       >>> t = PyKatus("teste1")

       >>> t.set_recebedor("29D4EB49-735E-429D-A5C3-B19DF50ADC47", "teste@teste.com")
       <teste1>
         <recebedor>
           <api_key>29D4EB49-735E-429D-A5C3-B19DF50ADC47</api_key>
           <email>teste@teste.com</email>
         </recebedor>
       </teste1>
       <BLANKLINE>
       
       >>> t.set_recebedor("", "teste@teste.com")
       Traceback (most recent call last):
       ValueError: Voce deve informar o token
       
       >>> t.set_recebedor("29D4EB49-735E-429D-A5C3-B19DF50ADC47", "t@teste.com")
       Traceback (most recent call last):
       ValueError: E-mail invalido
       
       >>> t = PyKatus("teste2")
       >>> t.set_produto("UFC153", "Cueca Velha", "2", "10.00", "2.00", "0.00", "0.00")
       <teste2>
         <produtos>
           <produto>
             <quantidade>2</quantidade>
             <peso>2.00</peso>
             <frete>0.00</frete>
             <preco>10.00</preco>
             <codigo>UFC153</codigo>
             <descricao>Cueca Velha</descricao>
             <desconto>0.00</desconto>
           </produto>
         </produtos>
       </teste2>
       <BLANKLINE>
       
       >>> t.set_produto("", "Cueca Velha", "2", "10.00", "2.00", "0.00", "0.00")
       Traceback (most recent call last):
       ValueError: Voce deve informar o codigo
       
       >>> t.set_produto("UFC153", "", "2", "10.00", "2.00", "0.00", "0.00")
       Traceback (most recent call last):
       ValueError: Voce deve informar uma descricao

       >>> t.set_produto("UFC153", "Cueca Velha", "", "10.00", "2.00", "0.00", "0.00")
       Traceback (most recent call last):
       ValueError: Voce deve informar uma quantidade

       >>> t.set_produto("UFC153", "Cueca Velha", "2", "", "2.00", "0.00", "0.00")
       Traceback (most recent call last):
       ValueError: Voce deve informar um preco

       >>> t.set_produto("UFC153", "Cueca Velha", "2", "10.00", "", "0.00", "0.00")
       Traceback (most recent call last):
       ValueError: Voce deve informar um peso

       >>> t.set_produto("UFC153", "Cueca Velha", "2", "10.00", "2.00", "", "0.00")
       Traceback (most recent call last):
       ValueError: Voce deve informar um frete

       >>> t.set_produto("UFC153", "Cueca Velha", "2", "10.00", "2.00", "0.00", "")
       Traceback (most recent call last):
       ValueError: Voce deve informar um desconto       
       """                     
if __name__=="__main__":
        import doctest      
        doctest.testmod()